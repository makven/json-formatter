﻿[
  {
    "_id": "589623797085dc5858c8aaee",
    "index": 0,
    "guid": "c2041bd1-5701-4cec-b06d-082eec63a337",
    "isActive": true,
    "balance": "$2,456.24",
    "picture": "http://placehold.it/32x32",
    "age": 29,
    "eyeColor": "green",
    "name": "Cathy Koch",
    "gender": "female",
    "company": "GEEKULAR",
    "email": "cathykoch@geekular.com",
    "phone": "+1 (947) 571-3950",
    "address": "154 Riverdale Avenue, Belvoir, Maine, 6218",
    "about": "Nisi tempor enim esse excepteur et dolor est ad incididunt do sunt esse eiusmod exercitation. Non aute excepteur dolore occaecat. Esse est aliqua in culpa minim id. Commodo pariatur duis magna quis laboris sint esse adipisicing cupidatat quis. Deserunt qui et non aliquip veniam consequat proident ad laborum ad ipsum eu.\r\n",
    "registered": "2014-06-16T07:39:47 -01:00",
    "latitude": -74.52405,
    "longitude": 116.247798,
    "tags": [
      "anim",
      "nostrud",
      "nostrud",
      "et",
      "et",
      "amet",
      "ipsum"
    ],
    "friends": [
      {
        "id": 0,
        "name": "Tucker Velez"
      },
      {
        "id": 1,
        "name": "Lindsey Mcfarland"
      },
      {
        "id": 2,
        "name": "Lessie Whitley"
      }
    ],
    "greeting": "Hello, Cathy Koch! You have 3 unread messages.",
    "favoriteFruit": "apple"
  },
  {
    "_id": "58962379770550ebbdd2db6a",
    "index": 1,
    "guid": "c36a9d27-a14a-4f82-acc1-9f83b4590996",
    "isActive": true,
    "balance": "$1,830.85",
    "picture": "http://placehold.it/32x32",
    "age": 36,
    "eyeColor": "brown",
    "name": "Benton Mccoy",
    "gender": "male",
    "company": "ZERBINA",
    "email": "bentonmccoy@zerbina.com",
    "phone": "+1 (827) 443-3507",
    "address": "855 Garland Court, Katonah, Alabama, 1090",
    "about": "Magna ut laborum nulla consectetur do. Sint do dolore culpa incididunt in sint culpa proident quis. Sint voluptate pariatur aliqua ut qui est. Ut amet consequat id aliquip. Nisi ipsum non occaecat exercitation id. Amet esse in elit nisi. Dolore qui duis enim incididunt ullamco excepteur nostrud cupidatat.\r\n",
    "registered": "2016-11-21T05:28:15 -00:00",
    "latitude": 29.48975,
    "longitude": -8.290184,
    "tags": [
      "excepteur",
      "proident",
      "ipsum",
      "dolor",
      "et",
      "et",
      "pariatur"
    ],
    "friends": [
      {
        "id": 0,
        "name": "Delgado Haney"
      },
      {
        "id": 1,
        "name": "Dudley Graves"
      },
      {
        "id": 2,
        "name": "Geneva Jenkins"
      }
    ],
    "greeting": "Hello, Benton Mccoy! You have 5 unread messages.",
    "favoriteFruit": "apple"
  },
  {
    "_id": "589623796863b17b934ae34d",
    "index": 2,
    "guid": "29279564-8ac5-47d2-82c1-de0b05c176d6",
    "isActive": false,
    "balance": "$3,088.50",
    "picture": "http://placehold.it/32x32",
    "age": 29,
    "eyeColor": "blue",
    "name": "Hyde Ramos",
    "gender": "male",
    "company": "FRENEX",
    "email": "hyderamos@frenex.com",
    "phone": "+1 (972) 543-2926",
    "address": "457 Lenox Road, Connerton, Indiana, 6178",
    "about": "Anim mollit nulla in exercitation. Sunt do pariatur veniam est do aliquip consectetur et reprehenderit aute elit id in. Id elit et ex culpa cupidatat adipisicing consequat nulla dolor id nostrud. Dolor nostrud cillum elit ad. Nisi eu commodo enim non in sit occaecat. Aliqua velit veniam incididunt sit incididunt.\r\n",
    "registered": "2015-05-08T12:40:53 -01:00",
    "latitude": 1.370448,
    "longitude": -109.563439,
    "tags": [
      "incididunt",
      "eiusmod",
      "aliquip",
      "ullamco",
      "ut",
      "ut",
      "aute"
    ],
    "friends": [
      {
        "id": 0,
        "name": "Bridgett Castillo"
      },
      {
        "id": 1,
        "name": "Goldie Middleton"
      },
      {
        "id": 2,
        "name": "Misty Curtis"
      }
    ],
    "greeting": "Hello, Hyde Ramos! You have 10 unread messages.",
    "favoriteFruit": "apple"
  },
  {
    "_id": "58962379eb199b8d86dce94a",
    "index": 3,
    "guid": "df17d765-e9a7-4da4-97dd-760330e53dc4",
    "isActive": false,
    "balance": "$1,964.84",
    "picture": "http://placehold.it/32x32",
    "age": 28,
    "eyeColor": "green",
    "name": "Juanita Harrell",
    "gender": "female",
    "company": "COMTRACT",
    "email": "juanitaharrell@comtract.com",
    "phone": "+1 (924) 560-2818",
    "address": "783 Cornelia Street, Weogufka, Illinois, 7060",
    "about": "Dolore ipsum quis nulla est amet fugiat Lorem. Consequat ex amet nisi nostrud enim duis qui exercitation adipisicing elit culpa consequat. Aliquip laboris eiusmod laborum tempor ut nostrud consequat culpa officia. Mollit ad id elit amet veniam. Non magna fugiat ullamco cupidatat tempor nisi Lorem adipisicing cillum. Mollit deserunt fugiat quis sunt nostrud officia ullamco nisi voluptate quis aute.\r\n",
    "registered": "2016-07-18T05:11:28 -01:00",
    "latitude": 74.703366,
    "longitude": -145.973155,
    "tags": [
      "non",
      "elit",
      "quis",
      "deserunt",
      "dolor",
      "veniam",
      "velit"
    ],
    "friends": [
      {
        "id": 0,
        "name": "Thornton Hopper"
      },
      {
        "id": 1,
        "name": "Huber Aguirre"
      },
      {
        "id": 2,
        "name": "Liliana Branch"
      }
    ],
    "greeting": "Hello, Juanita Harrell! You have 7 unread messages.",
    "favoriteFruit": "banana"
  },
  {
    "_id": "589623794eb80843034e9b44",
    "index": 4,
    "guid": "21b906ef-61e2-405b-8c9d-12420062e928",
    "isActive": true,
    "balance": "$1,359.77",
    "picture": "http://placehold.it/32x32",
    "age": 28,
    "eyeColor": "brown",
    "name": "Pratt Landry",
    "gender": "male",
    "company": "REALYSIS",
    "email": "prattlandry@realysis.com",
    "phone": "+1 (904) 502-3595",
    "address": "552 Havemeyer Street, Sena, South Carolina, 6963",
    "about": "Esse commodo cillum nisi in esse elit dolor pariatur incididunt ea. Eiusmod et irure ea proident ipsum tempor laboris in. Labore ex eu duis laborum consequat duis. Non ex proident commodo commodo tempor deserunt ipsum.\r\n",
    "registered": "2014-10-28T01:17:38 -00:00",
    "latitude": -38.38502,
    "longitude": 16.330211,
    "tags": [
      "cillum",
      "incididunt",
      "magna",
      "voluptate",
      "et",
      "sunt",
      "officia"
    ],
    "friends": [
      {
        "id": 0,
        "name": "Jaclyn Macias"
      },
      {
        "id": 1,
        "name": "Mclaughlin Deleon"
      },
      {
        "id": 2,
        "name": "Cleo Horn"
      }
    ],
    "greeting": "Hello, Pratt Landry! You have 10 unread messages.",
    "favoriteFruit": "strawberry"
  },
  {
    "_id": "58962379afcd486de7fee9ac",
    "index": 5,
    "guid": "e61b0005-ed77-42fb-9f72-a6c28729d571",
    "isActive": false,
    "balance": "$2,176.30",
    "picture": "http://placehold.it/32x32",
    "age": 31,
    "eyeColor": "blue",
    "name": "Gena Vaughan",
    "gender": "female",
    "company": "COMVEY",
    "email": "genavaughan@comvey.com",
    "phone": "+1 (891) 529-2097",
    "address": "515 Kenmore Terrace, Ivanhoe, Oregon, 5127",
    "about": "Esse magna eiusmod amet duis eiusmod magna fugiat laborum fugiat sunt sit. Id velit elit ea ea commodo adipisicing enim aute reprehenderit irure laborum. Cillum sunt in non ea esse voluptate do. Culpa veniam est ea eu aute laborum eu ex. Esse aliquip non dolore minim ex magna reprehenderit aliquip esse consectetur. Ipsum laborum reprehenderit magna nostrud minim nulla dolore in laboris velit.\r\n",
    "registered": "2016-11-07T02:22:13 -00:00",
    "latitude": -70.371983,
    "longitude": -157.976238,
    "tags": [
      "adipisicing",
      "dolor",
      "deserunt",
      "aute",
      "culpa",
      "elit",
      "pariatur"
    ],
    "friends": [
      {
        "id": 0,
        "name": "Shauna Tyson"
      },
      {
        "id": 1,
        "name": "Reynolds Oliver"
      },
      {
        "id": 2,
        "name": "Vanessa Joyner"
      }
    ],
    "greeting": "Hello, Gena Vaughan! You have 10 unread messages.",
    "favoriteFruit": "apple"
  }
]