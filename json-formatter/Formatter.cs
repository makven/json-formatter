﻿using System.Text;

namespace json_formatter
{
	public class Formatter
	{
		private readonly string intend;

		public Formatter(string intend = "\t")
		{
			this.intend = intend;
		}

		public string Format(string input)
		{
			var result = new StringBuilder();
			int level = 0;
			bool insideString = false;
			bool insideEmptyObject = false;

			for (var i = 0; i < input.Length; i++)
			{
				char c = input[i];

				if (c == '"')
				{
					insideString = UpdateInsideString(input, insideString, i);
				}

				if (!insideString)
				{
					if (char.IsWhiteSpace(c))
					{
						continue;
					}

					if (c == '}' || c == ']')
					{
						if (insideEmptyObject)
						{
							insideEmptyObject = false;
						}
						else
						{
							level--;
							this.BreakLine(result, level);
						}
					}
				}

				result.Append(c);

				if (insideString)
				{
					continue;
				}

				if (c == ':')
				{
					result.Append(' ');
				}

				if (c == ',')
				{
					this.BreakLine(result, level);
				}

				if (c == '{' || c == '[')
				{
					if (this.IsEmptyObject(c, i, input))
					{
						insideEmptyObject = true;
					}
					else
					{
						level++;
						this.BreakLine(result, level);
					}
				}
			}

			return result.ToString();
		}

		private bool IsEmptyObject(char c, int index, string input)
		{
			switch (c)
			{
				case '{':
					return index < input.Length && input[++index] == '}';
				case '[':
					return index < input.Length && input[++index] == ']';
				default:
					return false;
			}
		}

		private static bool UpdateInsideString(string input, bool insideString, int currentLocation)
		{
			if (!insideString)
			{
				insideString = true;
			}
			else
			{
				if (!IsEscaped(input, currentLocation))
				{
					insideString = false;
				}
			}
			return insideString;
		}

		private static bool IsEscaped(string input, int i)
		{
			i--;
			int count = 0;
			while (i >= 0 && input[i] == '\\')
			{
				i--;
				count++;
			}

			return count % 2 == 1;
		}

		private void BreakLine(StringBuilder result, int level)
		{
			result.AppendLine();
			for (int i = 0; i < level; i++)
			{
				result.Append(this.intend);
			}
		}
	}
}
